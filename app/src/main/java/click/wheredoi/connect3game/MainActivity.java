package click.wheredoi.connect3game;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    boolean isRedTurn = true;
    boolean isGameFinished = false;
    boolean redWon = false;
    boolean yellowWon = false;
    int clicks = 0;

    public void gridClick(View view) {
        if (isGameFinished) {
            Log.i(TAG, "A cell was clicked but game is finished!");
            return;
        } else if (view.getTag() != null) {
            Log.i(TAG, "A second click on a coin was prevented!");
            return;
        }
        clicks++;

        Log.i(TAG, String.valueOf(view.getId()));

        if (isRedTurn) {
            addRedCoin(view);
        } else {
            addYellowCoin(view);
        }
        checkThreeInARow(view);
    }

    private void addRedCoin(View view) {
        isRedTurn = false;
        String tag = getResources().getString(R.string.red);

        ((ImageView) view).setImageResource(R.drawable.red);
        view.setTag(tag);
        Log.i(TAG, String.format("Tag of RedCoin %s set to %s", view.getId(), tag));
        makeAnimation(view);
    }

    private void addYellowCoin(View view) {
        isRedTurn = true;
        String tag = getResources().getString(R.string.yellow);

        ((ImageView) view).setImageResource(R.drawable.yellow);
        view.setTag(tag);
        Log.i(TAG, String.format("Tag of YellowCoin %s set to %s", view.getId(), tag));
        makeAnimation(view);
    }

    private void makeAnimation(View view) {
        view.animate().alpha(1).setDuration(500);
    }

    private void removeAnimation(View view) {
        view.animate().alpha(0).setDuration(500);
    }

    public void resetGame(View view) {
        isRedTurn = true;
        isGameFinished = false;
        redWon = false;
        yellowWon = false;
        clicks = 0;
        GridLayout grid = findViewById(R.id.gridLayout);
        int imageViewCount = grid.getChildCount();

        for (int i=0; i < imageViewCount; i++) {
            ImageView iv = (ImageView) grid.getChildAt(i);
            removeAnimation(iv);

            if (iv.getTag() != null) {
                iv.setTag(null);
            }
        }

        toggleVisibility();
        Log.i(TAG, "GAME RESET");
    }

    /**
     * Checks if one of the rows has three of the same coins
     */
    private void checkThreeInARow(View view) {
        if (clicks < 3) {
            return;
        } else if (clicks == 9) {
            finishGame(false);
        }

        View iv1 = findViewById(R.id.imageView5);
        View iv2 = findViewById(R.id.imageView6);
        View iv3 = findViewById(R.id.imageView7);
        View iv4 = findViewById(R.id.imageView8);
        View iv5 = findViewById(R.id.imageView9);
        View iv6 = findViewById(R.id.imageView10);
        View iv7 = findViewById(R.id.imageView11);
        View iv8 = findViewById(R.id.imageView12);
        View iv9 = findViewById(R.id.imageView13);
        
        Log.i(TAG, String.format("checkThreeInARow(): all views: \n" +
                        "%s\n" + "%s\n" + "%s\n" + "%s\n" + "%s\n" +
                        "%s\n" + "%s\n" + "%s\n" + "%s\n",
                iv1, iv2, iv3, iv4, iv5, iv6, iv7, iv8, iv9));

        // All possible three in a rows
        if (isRowTheSame(iv1, iv2, iv3)) {
            finishGame(true);
        } else if (isRowTheSame(iv4, iv5, iv6)) {
            finishGame(true);
        } else if (isRowTheSame(iv7, iv8, iv9)) {
            finishGame(true);
        } else if (isRowTheSame(iv1, iv4, iv7)) {
            finishGame(true);
        } else if (isRowTheSame(iv2, iv5, iv8)) {
            finishGame(true);
        } else if (isRowTheSame(iv3, iv6, iv9)) {
            finishGame(true);
        } else if (isRowTheSame(iv1, iv5, iv9)) {
            finishGame(true);
        } else if (isRowTheSame(iv3, iv5, iv7)) {
            finishGame(true);
        }
    }

    private void finishGame(boolean hasWinner) {
        isGameFinished = true;
        TextView tv = findViewById(R.id.textView);
        Resources r = getResources();

        if (hasWinner && redWon) {
            tv.setText(r.getString(R.string.won, r.getString(R.string.red)));
        } else if (hasWinner && yellowWon) {
            tv.setText(r.getString(R.string.won, r.getString(R.string.yellow)));
        } else {
            tv.setText(r.getString(R.string.nobodywon));
        }
        toggleVisibility();
    }

    private void toggleVisibility() {
        View msg = findViewById(R.id.textView);
        View btn = findViewById(R.id.button);

        Log.i(TAG, "Button visibility: " + btn.getVisibility());

        if (btn.getVisibility() == View.INVISIBLE) {
            msg.setVisibility(View.VISIBLE);
            btn.setVisibility(View.VISIBLE);
        } else {
            msg.setVisibility(View.INVISIBLE);
            btn.setVisibility(View.INVISIBLE);
        }
    }


    private boolean isRedTag(View view) {
        boolean result = false;

        if (view.getTag() != null) {
            Log.i(TAG, "isRedTag(): tag of coin: " + view.getTag().toString());
            result = view.getTag().toString().equals(getResources().getString(R.string.red));
            Log.i(TAG, "Comparison: " + result);
        } else {
            Log.i(TAG, "isRedTag() was skipped because getTag() was null");
        }
        return result;
    }

    private boolean isYellowTag(View view) {
        boolean result = false;

        if (view.getTag() != null) {
            Log.i(TAG, "isYellowTag(): tag of coin: " + view.getTag().toString());
            result = view.getTag().toString().equals(getResources().getString(R.string.yellow));
            Log.i(TAG, "Comparison: " + result);
        } else {
            Log.i(TAG, "isYellowTag() was skipped because getTag() was null");
        }
        return result;
    }

    private boolean isRowTheSame(View first, View second, View third) {
        Log.i(TAG, "isRowTheSame() is called");
        Log.i(TAG, String.format("Arguments:\n%s\n%s\n%s", first, second, third));

        if (isRedTag(first) && isRedTag(second) && isRedTag(third)) {
            Log.i(TAG, "Red has three in a row!");
            redWon = true;
            return true;
        } else if (isYellowTag(first) && isYellowTag(second) && isYellowTag(third)) {
            Log.i(TAG, "Yellow has three in a row!");
            yellowWon = true;
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
